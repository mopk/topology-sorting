class TopologySort {

    static void resetCounters() {
        nodesProcessed = 0
        edgesProcessed1 = 0
        edgesProcessed2 = 0
    }

    static void printCounters() {
        println 'Nodes processed: ' + nodesProcessed
        println 'Edges processed 1: ' + edgesProcessed1
        println 'Edges processed 2: ' + edgesProcessed2
    }

    static int nodesProcessed = 0
    static int edgesProcessed1 = 0
    static int edgesProcessed2 = 0

    /**
     * The graph parameter is under our control from the very beginning
     * so let's skip building it. It's already built.
     *
     * Input size is L + O = N nodes and E edges where L stands for leaves
     * and O stands for other nodes. H is the number of levels of input
     * graph counting from leaves to the most dependent nodes.
     *
     * @return list ordered the way nodes of DAG can be processed most
     *          effectively (in fact it is a queue to be processed).
     *          It's enough to return list here as the list can be used in
     *          conjunction with the input list of leaves (the only input
     *          parameter).
     */
    static List<Node> topSort(List<Node> leaves) {
        List<Node> resultQueue = []

        // leaves can be enqueued from the very beginning
        leaves.each {resultQueue << it }

        List<Node> currentLevel = leaves
//        println leaves
        while (true) {         // executed H times, the biggest H is when H = N
            currentLevel = upOneLevel(currentLevel)
//            println 'CL: ' + currentLevel
            if (!currentLevel) {
                break
            }
            resultQueue.addAll currentLevel
        }

        printCounters()
        resetCounters()

        resultQueue
    }

    /**
     *
     * @param nodes of the same level of reachability from the leaves
     * @return input nodes subset to be enqueued
     */
    static List<Node> upOneLevel(List<Node> nodes) {
        List<Node> toBeEnqueued = []

        // In the case this will work correctly even without hashCode and equals
        // defined as all the nodes are unique and never copied/cloned in the
        // scope of the task. Left this way for now for the sake of brevity
        // mostly while can be fragile maintenance-wise.
        //
        Set<Node> uniqueDependants = new HashSet<>()

        // O(E) pick, add and remove operations made to collections in total
        // considering outer loop
        //
        // However, worst case graph having only one node at each level and
        // each node having (its_current_level - 1) of outgoing edges going to
        // exactly each of the further levels would be:
        // 4V->6E, 5V->10E, 6V->15E, 7V->21E, 8V->28E
        // and its series:
        // (V-1) + (V-2) + ... + (V-(V-2)) + (V-(V-1)) =
        // = (V-1) + (V-2) + ... + 2 + 1
        // but this is the sum of the first (V-1) or N natural numbers
        // = (V-1)*((V-1) + 1)/2 or n*(n+1)/2
        // and for E edges E=(V-1)*V/2 because the arithmetic progression
        // of the vertices has E-1 members.
        // And the above implies O(V^2) adds and removes.
        // But it's still O(E+V) which is equivalent to that of DFS.
        //
        // Each graph node is processed here only once.
        nodes.each {node ->
//            println 'N: ' + node
            nodesProcessed++

            // 2021-12-04:
            // Being a nested loop is what may make one believe the
            // algorithm drafted has O(V^2 ...).
            // The fact is that this loop is not actually over nodes
            // but edges in terms of well known graph algorithms. The way the
            // graph is represented assumes an edge is a pair of nodes.
            //
            // Each graph edge is processed here only once.
            node.dependants.each { dependant ->
//                println 'D: ' + dependant
                edgesProcessed1++

                // equivalent to Kahn's algorithm marking making O(1) for an
                // occurrence of unlinking
                dependant.unlinkDependencyOnly(node)

                uniqueDependants << dependant
            }
        }

//        println 'UD: ' + uniqueDependants

        // O(E) picks, checks and adds considering outer loop as well
        //
        // Again, while it looks like we iterate over vertices while in fact we do
        // that over edges with the assumed graph representation.
        // The fact the number of edges E can be as large as V^2 is
        // acceptable in graphs generally.
        //
        // Each graph node is processed here only once.
        uniqueDependants.each {
            edgesProcessed2++

            if (it.dependencies.isEmpty()) {
                toBeEnqueued << it
            }
        }
        toBeEnqueued
    }

    static void main(String[] args) {
        def a, b, c, d, e
        List<Node> leaves

        // an unbalanced tree
        a = new Node("a")
        b = new Node("b")
        c = new Node("c")
        d = new Node("d")
        b.linkDependency(a)
        c.linkDependency(b)
        c.linkDependency(d)
        leaves = [a, d]
        println topSort(leaves)

        // a diamond
        a = new Node("a")
        b = new Node("b")
        c = new Node("c")
        d = new Node("d")
        a.linkDependency(b)
        d.linkDependency(a)
        d.linkDependency(c)
        c.linkDependency(b)
        assert b.dependants == [a, c] // not result tests
        Set<Node> dDeps = new HashSet<>()
        dDeps.addAll([a, c])
        assert d.dependencies == dDeps
        leaves = [b]
        println topSort(leaves)

        // a forest of two two-element chains
        a = new Node("a")
        b = new Node("b")
        c = new Node("c")
        d = new Node("d")
        a.linkDependency(b)
        c.linkDependency(d)
        assert b.dependants == [a] // not result tests
        assert d.dependants == [c]
        leaves = [b, d]
        println topSort(leaves)

        // a diamond with a trivial loop (back and forth the same edge)
        a = new Node("a")
        b = new Node("b")
        c = new Node("c")
        d = new Node("d")
        a.linkDependency(b)
        d.linkDependency(a)
        d.linkDependency(c)
        c.linkDependency(b)
        c.linkDependency(d) // completes a trivial loop
        leaves = [b]
        println topSort(leaves)

        // an edge leading to a triangle loop
        a = new Node("a")
        b = new Node("b")
        c = new Node("c")
        d = new Node("d")
        b.linkDependency(a)
        c.linkDependency(b)
        d.linkDependency(c)
        b.linkDependency(d) // completes a triangle loop b -> c -> d -> b
        leaves = [a]
        println topSort(leaves)

        // Reasonable unit tests can be built asserting partial ordering which
        // will duplicate the graph structure (edges and transitive links).
        // I assume the debug output is enough for now to verify if the solution
        // provided is good enough even while not having validations and
        // defensive programming elements implemented.
    }
}

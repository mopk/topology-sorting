/**
 * Node class is enough to represent a graph because a collection of Node
 * instances can easily represent the set of leaves of the graph. And the
 * leaves are linked with the rest of the nodes of the graph meaning the rest
 * of the nodes is reachable via the leaves.
 */
class Node {

    // unique node label to make debug output understandable
    String label

    Set<Node> dependencies = new HashSet<>()
    List<Node> dependants = []

    Node(String label) {
        this.label = label
    }

    void linkDependency(Node dependency) {
        dependencies << dependency
        dependency.dependants << this
    }

    // Remove below can be replaced by something preserving graph structure by
    // something like moving the dependency edge to another collection field.
    // But even in the current state serves well the idea making the algorithm
    // linear.
    boolean unlinkDependencyOnly(Node dependency) {
        dependencies.remove(dependency)
    }

    @Override
    String toString() {
        return "Node{" +
                "label='" + label + '\'' +
                '}';
    }
}
